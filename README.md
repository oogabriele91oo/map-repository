in this repository with react: https://reactjs.org/docs/create-a-new-react-app.html
and dedicated libraries:
- react-azure-map (https://azure.github.io/react-azure-maps/index.html)
- react-google-map (https://developers.google.com/maps/documentation/javascript/react-map?hl=en)
- mapbox-gl (https://docs.mapbox.com/help/tutorials/use-mapbox-gl-js-with-react/)

I have developed a single page app to view a map and be able to insert markers indicating the position.
Within the code I have implemented several maps and solutions that can be shown or changed from the default one (google maps) to ensure maximum usability in case of problems in loading the default one.

The map can be integrated into countless applications and / or solutions.