import { Wrapper, Status } from "@googlemaps/react-wrapper";

import GoogleMap from "../google-map/GoogleMap";


const render = (status) => {
  if (status === Status.LOADING) return "Loading...";
  if (status === Status.FAILURE) return "Error";
  return null;
};

export default function MapWrapper() {
  return (
    <Wrapper apiKey={"AIzaSyCsbia1GnxCxXlLo6QM53ftt0iuK8vEtDY"} render={render} >
      <GoogleMap />
    </Wrapper>
  );
};


