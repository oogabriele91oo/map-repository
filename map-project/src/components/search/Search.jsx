import { useState } from "react";

export default function Search(props) {
  const geocoder = new window.google.maps.Geocoder();
  const { setCenter } = props;
  const [address, setAddress] = useState("");

  const onSearch = () => {
    geocoder
      .geocode({ address })
      .then((result) => {
        const { results } = result;
        const firstResult = results[0];
        const location = firstResult.geometry.location;
        const lat = location.lat();
        const lng = location.lng();
        setCenter({ lat, lng });
      })
      .catch((e) => {
        alert("Geocode was not successful for : " + e);
      });
  };

  return (
    <div className="row">
      <input
        className="form-control"
        placeholder="Street, Location or Coordinate"
        type="text"
        value={address}
        onChange={(e) => setAddress(e.target.value)}
      />
      <button className="btn btn-outline-secondary" onClick={onSearch}>Search</button>
    </div>
  );
};
