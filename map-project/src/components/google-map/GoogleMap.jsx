import { useEffect, useRef, useState } from "react";

import Search from "../search/Search";

export default function GoogleMap() {
  const ref = useRef(null);
  const [map, setMap] = useState();
  const [center, setCenter] = useState({ lat: 41.87279, lng: 12.47731 });

  useEffect(() => {
    if (ref.current && !map) {
      setMap(new window.google.maps.Map(ref.current, {}));
    }
  }, [ref, map]);

  // marker
  useEffect(() => {
    if (map) {
      map.setOptions({ center, zoom: 5 });
    }
    new window.google.maps.Marker({
      position: center,
      map
    });
  }, [map, center]);

  return (
    <div className="container">
      <Search setCenter={setCenter} />
      <div style={{ height: "80px" }} />
      <div ref={ref} style={{ width: "150%", height: "800%" }} />
    </div>
  );
}