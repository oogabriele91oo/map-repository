import React from "react";
import Map from "../map/Map";
import { carData } from "../../functions/data";
import { AzureMap } from "react-azure-maps";


export default class Form extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            address: '',
            name: '',
            lat: '',
            lng: ''
        };

    }
    
    handleChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleSubmit = e => {
        e.preventDefault()
        let Data = { carData };
        Data.push( {
            name: this.state.name,
            address: this.state.address,
            position: {
              latitude: this.state.lat,
              speed: null,
              longitude: this.state.lng
            }
        });
    }

    render() {
        return (
            <div className="row container">
                <div className="col">
                    <div>
                        <Map />
                    </div>
                </div>
                <div className="col d-flex align-items-center">
                    <form onSubmit={this.handleSubmit}>
                        <h1>Markers Creator</h1>
                        <div className="form-group">
                            <label>Name</label>
                            <input type="text" name="name" className="form-control"  value={this.state.name} onChange={this.handleChange} placeholder="Name of Place" required />
                        </div>
                        <div className="form-group">
                            <label>Address</label>
                            <input type="text" name="address" className="form-control"  value={this.state.address} onChange={this.handleChange} placeholder="Street" required />
                        </div>
                        <div className="form-group">
                            <label>Latitude</label>
                            <input type="number" name="lat" className="form-control" value={this.state.lat} onChange={this.handleChange} placeholder="Lat" required />
                        </div>
                        <div className="form-group">
                            <label>Longitude</label>
                            <input type="number" name="lng" className="form-control" value={this.state.lng} onChange={this.handleChange} placeholder="Lng" required />
                        </div>
                        <button type="submit" className="btn btn-primary btn-block">Create</button>
                    </form>
                </div>
            </div>
        );
    }


}