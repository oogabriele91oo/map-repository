import 'bootstrap/dist/js/bootstrap';
import Form from './components/form/Form';
import './App.css';
import Map from './components/map/Map';
import { Component } from 'react';
import { AzureMap } from 'react-azure-maps';
import MapWrapper from './components/map-wrapper/MapWrapper';


function App() {
  return (
    <div className="App p-1">
      <h1 className="display-2">My Google Maps with Markers</h1>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          height: "70vh",
          width: "70vw"
        }}
      >
        <MapWrapper />
      </div>
  </div>
  );
}

export default App;
